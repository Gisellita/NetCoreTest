﻿using System;
using System.Linq;
using Dominio.Contexto;
using Dominio.Entidades;
using Dominio.IntegracionConOtrosServicios.INEI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dominio.Pruebas
{
    [TestClass]
    public class ContextoDeDatos
    {
        [TestMethod]
        public void Creacion()
        {
            using (TiendaBD db = new TiendaBD())
            {
                db.Productos.ToList();
            }
        }

        [TestMethod]
        public void AgregarCategoria()
        {
            using (TiendaBD db = new TiendaBD())
            {
                Categoria categoria = new Categoria();
                categoria.Nombre = "Frutas";
                db.Categorias.Add(categoria);
                db.SaveChanges();
            }
        }

        [TestMethod]
        public void CargarUbigeos()
        {
            using (TiendaBD db = new TiendaBD())
            {
                var ubigeos = DistribucionPoliticaDePeru.SacarUbigeosDeINEI();
                db.Ubigeos.AddRange(ubigeos);
                db.SaveChanges();
            }
        }

        [TestMethod]
        public void AgregarProductos()
        {
            using (TiendaBD db = new TiendaBD())
            {
                Categoria categoria = new Categoria();
                categoria.Nombre = "Vegetales";
                db.Categorias.Add(categoria);

                db.Productos.Add(new Producto()
                    {
                        Nombre = "Cebolla",
                        Precio = 1,
                        Categoria = categoria
                    }
                );

                Producto producto2 = new Producto();
                producto2.Nombre = "Maiz";
                producto2.Precio = 2;
                db.Productos.Add(producto2);

                db.SaveChanges();

                var categoriaVegetal = db.Categorias.Include("Productos").FirstOrDefault(x => x.Nombre.Equals("Vegetales"));
                Assert.AreEqual(2, categoriaVegetal.Productos.Count);

                var queryDeCategory = db.Categorias.AsQueryable();


                queryDeCategory = queryDeCategory.Where(x => x.Id > 2);

                var categorias = queryDeCategory.ToList();

            }
        }
    }
}
