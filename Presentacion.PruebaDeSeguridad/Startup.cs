﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Presentacion.PruebaDeSeguridad.Startup))]
namespace Presentacion.PruebaDeSeguridad
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
