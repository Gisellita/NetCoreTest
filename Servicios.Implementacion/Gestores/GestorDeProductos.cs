﻿using Dominio.Contexto;
using Dominio.Entidades;
using Servicios.Interfaces.Producto;
using Servicios.Interfaces.Producto.Peticiones;
using Servicios.Interfaces.Producto.Respuesta;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Implementacion.Gestores
{
    public class GestorDeProductos : IGestorDeProductos
    {
        public ProductoGeneral Actualizar(ActualizarProducto peticionDeActualizacion)
        {
            using (TiendaBD db = new TiendaBD())
            {
                Producto producto = db.Productos.Find(peticionDeActualizacion.Id);
                producto.CategoriaId = peticionDeActualizacion.CategoriaId;
                producto.Precio = peticionDeActualizacion.Precio;
                db.SaveChanges();


                return db.Productos
                         .Where(x => x.Id.Equals(producto.Id))
                         .Select(x => new ProductoGeneral()
                         {
                             Nombre = x.Nombre,
                             Id = x.Id,
                             NombreCategoria = x.Categoria.Nombre,
                             Precio = x.Precio
                         })
                         .FirstOrDefault();
            }
        }

        public void Borrar(int Id)
        {
            using (TiendaBD db = new TiendaBD())
            {
                Producto producto =new Producto() { Id = Id };
                db.Productos.Attach(producto);
                db.Entry(producto).State = EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public ProductoGeneral Crear(NuevoProducto peticionDeCreacion)
        {
            using (TiendaBD db = new TiendaBD())
            {
                db.Productos.fir
                Producto producto = new Producto();
                producto.CategoriaId = peticionDeCreacion.CategoriaId;
                producto.Nombre = peticionDeCreacion.Nombre;
                producto.Precio = peticionDeCreacion.Precio;
                db.Productos.Add(producto);
                db.SaveChanges();

                return db.Productos
                         .Where(x => x.Id.Equals(producto.Id))
                         .Select(x => new ProductoGeneral()
                         {
                             Nombre = x.Nombre,
                             Id = x.Id,
                             NombreCategoria = x.Categoria.Nombre,
                             Precio = x.Precio
                         })
                         .FirstOrDefault();
            }
        }

        public List<ProductoGeneral> Listar()
        {
            using (TiendaBD db = new TiendaBD())
            {
                return db.Productos
                         .Select(x=> new ProductoGeneral() {
                             Nombre = x.Nombre,
                             Id = x.Id,
                             NombreCategoria = x.Categoria.Nombre,
                             Precio = x.Precio
                         })
                         .ToList();
            }
        }
    }
}
