﻿using Dominio.BaseDeDatos;
using Dominio.Contexto;
using Servicios.Interfaces.Pais.UbigeosPorPartes;
using Servicios.Interfaces.Pais.UbigeosPorPartes.Respuestas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Implementacion.Gestores
{
    public class GestorDeUbigeosPorPartes : IGestorDeUbigeosPorPartes
    {
        public List<Departamento> ListarDepartamentos()
        {
            using (TiendaBD peruDB = new TiendaBD())
            {
                return peruDB.Ubigeos
                             .Select
                             (
                                    x => new Departamento()
                                    {
                                        Id = x.Id.Substring(0, 2),
                                        Nombre = x.NombreDeDepartamento
                                    }
                             )
                             .Distinct()
                             .ToList();
            }
        }

        public List<Distrito> ListarDistritos(string IdDepartamento, string IdProvincia)
        {
            using (TiendaBD peruDB = new TiendaBD())
            {
                return peruDB.Ubigeos
                             .Where
                             (
                                    x => x.Id.StartsWith(IdDepartamento + IdProvincia)
                             )
                             .Select
                             (
                                    x => new Distrito()
                                    {
                                        Id = x.Id.Substring(4, 2),
                                        Nombre = x.NombreDeDistrito
                                    }
                             )
                             .Distinct()
                             .ToList();
            }
        }

        public List<Provincia> ListarProvincias(string IdDepartamento)
        {
            using (TiendaBD peruDB = new TiendaBD())
            {
                return peruDB.Ubigeos
                             .Where
                             (
                                    x=> x.Id.StartsWith(IdDepartamento)
                             )
                             .Select
                             (
                                    x => new Provincia()
                                    {
                                        Id = x.Id.Substring(2, 2),
                                        Nombre = x.NombreDeProvincia
                                    }
                             )
                             .Distinct()
                             .ToList();
            }
        }
    }
}
