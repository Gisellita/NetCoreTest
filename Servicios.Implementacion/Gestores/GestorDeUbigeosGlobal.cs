﻿using Dominio.Contexto;
using Servicios.Interfaces.Pais.UbigeosGlobal;
using Servicios.Interfaces.Pais.UbigeosGlobal.Respuestas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Implementacion.Gestores
{
    public class GestorDeUbigeosGlobal : IGestorDeUbigeosGlobal
    {
        public List<Departamento> GenerarPais()
        {
            using (TiendaBD peruDB = new TiendaBD())
            {
                List<Departamento> listaDeDepartamentos = new List<Departamento>();
                peruDB.Ubigeos.ToList().ForEach(ubigeo =>
                {
                    Departamento departamento = listaDeDepartamentos.FirstOrDefault(x => x.Nombre.Equals(ubigeo.NombreDeDepartamento));
                    if (departamento == null)
                    {
                        departamento = new Departamento();
                        departamento.Nombre = ubigeo.NombreDeDepartamento;
                        listaDeDepartamentos.Add(departamento);
                    }
                    Provincia provincia = departamento.Provincias.FirstOrDefault(x=>x.Nombre.Equals(ubigeo.NombreDeProvincia));
                    if (provincia == null)
                    {
                        provincia = new Provincia();
                        provincia.Nombre = ubigeo.NombreDeProvincia;
                        departamento.Provincias.Add(provincia);
                    }
                    Distrito distrito = new Distrito();
                    distrito.Nombre = ubigeo.NombreDeDistrito;
                    distrito.Id = ubigeo.Id;
                    provincia.Distritos.Add(distrito);

                });
                return listaDeDepartamentos;
            }
        }
    }
}
