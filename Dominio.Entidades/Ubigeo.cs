﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Entidades
{
    public class Ubigeo
    {
        public string Id { get; set; }
        public string NombreDeDepartamento { get; set; }
        public string NombreDeProvincia { get; set; }
        public string NombreDeDistrito { get; set; }
    }
}
