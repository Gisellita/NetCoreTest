﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentacion.Web.Hubs
{

    [HubName("ChatHub")]
    public class ChatHub:Hub
    {
        public void EnviarMensajeGlobal(string mensaje)
        {
            Clients.All.MensajeGlobal(mensaje);
        }
    }
}