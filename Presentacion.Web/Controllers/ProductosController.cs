﻿using Dominio.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentacion.Web.Controllers
{
    public class ProductosController : Controller
    {
        // GET: Productos
        public ActionResult Index()
        {
            using (TiendaBD db = new TiendaBD())
            {
                var productos = db.Productos.Include("Categoria").ToList();
                return View(productos);
            }     
        }
    }
}