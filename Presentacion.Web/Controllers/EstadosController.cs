﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentacion.Web.Controllers
{
    public class EstadosController : Controller
    {
        // GET: Estados
        public ActionResult Index()
        {
            System.Web.HttpContext.Current.Application["NombreApplicacion"] = "Se accesp al index";
            return View();
        }

        // GET: Estados
        public ActionResult CrearVariableDeSession(string Id)
        {
            Session["Nombre"] = Id;
            return View();
        }

        // GET: Estados
        public ActionResult CrearVariableDeApplicacion(string Id)
        {
            System.Web.HttpContext.Current.Application["NombreApplicacion"] = Id;
            return View();
        }


    }
}