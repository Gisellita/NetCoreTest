﻿using Presentacion.Web.ActionFilters;
using Servicios.Implementacion.Gestores;
using Servicios.Interfaces.Producto;
using Servicios.Interfaces.Producto.Peticiones;
using Servicios.Interfaces.Producto.Respuesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentacion.Web.Controllers.Api
{
    public class ProductosEnCapasController : ApiController
    {
        IGestorDeProductos _gestorDeProductos = new GestorDeProductos();

        [HttpGet]
        public List<ProductoGeneral> Listar()
        {
            return _gestorDeProductos.Listar();
        }


        [HttpPost]
        [FiltroDeManejoDeExcepciones]
        public ProductoGeneral Crear(NuevoProducto nuevoProducto)
        {
            return _gestorDeProductos.Crear(nuevoProducto);
        }

        [HttpPost]
        public ProductoGeneral Actualizar(ActualizarProducto actualizarProducto)
        {
            return _gestorDeProductos.Actualizar(actualizarProducto);
        }

        [HttpDelete]
        public void Borrar(int Id)
        {
            _gestorDeProductos.Borrar(Id);
        }

    }
}
