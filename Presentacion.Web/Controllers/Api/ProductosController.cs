﻿using Dominio.Contexto;
using Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentacion.Web.Controllers.Api
{
    public class ProductosController : ApiController
    {
        public List<Producto> Get()
        {
            using (TiendaBD db = new TiendaBD())
            {
                return db.Productos.ToList();
            }
        }
    }
}
