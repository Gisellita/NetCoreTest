﻿using Servicios.Implementacion.Gestores;
using Servicios.Interfaces.Pais.UbigeosPorPartes;
using Servicios.Interfaces.Pais.UbigeosPorPartes.Respuestas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentacion.Web.Controllers.Api
{
    public class PaisController : ApiController
    {
        private IGestorDeUbigeosPorPartes _gestorDeUbigeosPorPartes;

        public PaisController() {
            this._gestorDeUbigeosPorPartes = new GestorDeUbigeosPorPartes();
        }

        [HttpGet]
        public List<Departamento> ListarDepartamentos()
        {
            return _gestorDeUbigeosPorPartes.ListarDepartamentos();
        }

        [HttpGet]
        public List<Distrito> ListarDistritos(string IdDepartamento, string IdProvincia)
        {
            return _gestorDeUbigeosPorPartes.ListarDistritos( IdDepartamento, IdProvincia);
        }

        [HttpGet]
        public List<Provincia> ListarProvincias(string IdDepartamento)
        {
            return _gestorDeUbigeosPorPartes.ListarProvincias(IdDepartamento);
        }
    }
}
