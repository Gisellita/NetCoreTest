﻿using Servicios.Implementacion.Gestores;
using Servicios.Interfaces.Pais.UbigeosGlobal;
using Servicios.Interfaces.Pais.UbigeosGlobal.Respuestas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentacion.Web.Controllers.Api
{
    public class TodoElPaisController : ApiController
    {
        IGestorDeUbigeosGlobal _gestorDeUbigeosGlobal;
        private static List<Departamento> paisCacheado = null;

        public TodoElPaisController()
        {
            this._gestorDeUbigeosGlobal = new GestorDeUbigeosGlobal();
        }

        [HttpGet]
        public List<Departamento> Generar()
        {
            if (paisCacheado == null)
            {
                paisCacheado = this._gestorDeUbigeosGlobal.GenerarPais();
            }
            return paisCacheado;
        }
    }
}
