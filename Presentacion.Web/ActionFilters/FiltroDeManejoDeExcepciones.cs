﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace Presentacion.Web.ActionFilters
{
    public class FiltroDeManejoDeExcepciones : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            Debug.WriteLine("=================================");
            Debug.WriteLine(actionExecutedContext.Exception);
            Debug.WriteLine("=================================");
        }
    }
}