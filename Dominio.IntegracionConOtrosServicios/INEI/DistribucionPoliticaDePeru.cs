﻿using Dominio.Entidades;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.IntegracionConOtrosServicios.INEI
{
    public class DistribucionPoliticaDePeru
    {
        private static readonly string INEI_UBIGEO_URL = @"http://webinei.inei.gob.pe:8080/sisconcode/web/ubigeo/listaBusquedaUbigeoPorUbicacionGeograficaXls/5/1/1/null/null/null";
        private static readonly string NOMBRE_DE_HOJA = @"ubicacionGeografica";
        private static readonly int COLUMNA_DEPARTAMENTO = 1;
        private static readonly int COLUMNA_PROVINCIA = 4;
        private static readonly int COLUMNA_DISTRITO = 5;
        private static readonly string TITULO_DE_DEPARTAMENTO = "DEPARTAMENTO";

        public static List<Ubigeo> SacarUbigeosDeINEI()
        {
            using(var client = new WebClient())
            {
                byte[] data = client.DownloadData(INEI_UBIGEO_URL);
                MemoryStream archivoDeExcelEnMemoria = new MemoryStream(data);
                HSSFWorkbook libroDeExcel = new HSSFWorkbook(archivoDeExcelEnMemoria);
                ISheet sheet = libroDeExcel.GetSheet(NOMBRE_DE_HOJA);
                return parsearUbigeosDesdeLaHojaDeExcel(sheet);
            }
        }

        private static List<Ubigeo> parsearUbigeosDesdeLaHojaDeExcel(ISheet sheet)
        {
            List<Ubigeo> listaDeUbigeos = new List<Ubigeo>();
            for (int numeroDeFila = 0; numeroDeFila <= sheet.LastRowNum; numeroDeFila++)
            {
                var fila = sheet.GetRow(numeroDeFila);
                if (isAValidRow(fila))
                {
                    Ubigeo ubigeo = generateUbigeoFromRow(fila);
                    listaDeUbigeos.Add(ubigeo);
                }
            }
            return listaDeUbigeos;
        }

        private static Ubigeo generateUbigeoFromRow(IRow row)
        {
            Ubigeo ubigeo = new Ubigeo();
            string deparmentStringInformation = row.GetCell(COLUMNA_DEPARTAMENTO).StringCellValue;
            string pronvinceStringInformation = row.GetCell(COLUMNA_PROVINCIA).StringCellValue;
            string districtStringInformation = row.GetCell(COLUMNA_DISTRITO).StringCellValue;
            string deparmentName = getUbigeoNameCompoment(deparmentStringInformation);
            string pronvinceName = getUbigeoNameCompoment(pronvinceStringInformation);
            string districtName = getUbigeoNameCompoment(districtStringInformation);
            string deparmentCode = getUbigeoCodeCompoment(deparmentStringInformation);
            string pronvinceCode = getUbigeoCodeCompoment(pronvinceStringInformation);
            string districtCode = getUbigeoCodeCompoment(districtStringInformation);
            ubigeo.NombreDeDepartamento = deparmentName;
            ubigeo.NombreDeDistrito = districtName;
            ubigeo.NombreDeProvincia = pronvinceName;
            ubigeo.Id = deparmentCode + pronvinceCode + districtCode;
            return ubigeo;
        }


        private static string getUbigeoCodeCompoment(string stringInformation)
        {
            return stringInformation.Split(' ')[0];
        }

        private static string getUbigeoNameCompoment(string stringInformation)
        {
            string NameComponent = "";
            foreach (string nameSubComponent in stringInformation.Split(' ').Skip(1))
            {
                NameComponent += " " + nameSubComponent;
            }
            return NameComponent.Trim();
        }

        private static bool isAValidRow(IRow rowData)
        {
            if (rowData == null) return false;
            string deparmentStringData = rowData.GetCell(COLUMNA_DEPARTAMENTO).StringCellValue;
            string districtData = rowData.GetCell(COLUMNA_DISTRITO).StringCellValue.Trim();
            if (deparmentStringData.Contains(TITULO_DE_DEPARTAMENTO)) return false;
            if (String.IsNullOrEmpty(districtData)) return false;
            return true;
        }


    }
}
