﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Servicios.Interfaces.Producto.Respuesta;
using Servicios.Interfaces.Producto.Peticiones;
using Servicios.Interfaces.Compartido;

namespace Servicios.Interfaces.Producto
{
    public interface IGestorDeProductos : IMantenimientoBasico<NuevoProducto,ActualizarProducto, ProductoGeneral>
    {
    }
}
