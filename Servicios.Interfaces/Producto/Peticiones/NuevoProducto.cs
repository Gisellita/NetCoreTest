﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces.Producto.Peticiones
{
    public class NuevoProducto
    {
        public string Nombre { get; set; }
        public double Precio { get; set; }
        public int CategoriaId { get; set; }
    }
}
