﻿using Servicios.Interfaces.Pais.UbigeosPorPartes.Respuestas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces.Pais.UbigeosPorPartes
{
    public interface IGestorDeUbigeosPorPartes
    {
        List<Departamento> ListarDepartamentos();
        List<Provincia> ListarProvincias(string IdDepartamento);
        List<Distrito> ListarDistritos(string IdDepartamento,string IdProvincia);
    }
}
