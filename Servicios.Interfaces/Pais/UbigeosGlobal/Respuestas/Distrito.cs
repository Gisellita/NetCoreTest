﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces.Pais.UbigeosGlobal.Respuestas
{
    public class Distrito
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }
}
