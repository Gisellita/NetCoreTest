﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces.Pais.UbigeosGlobal.Respuestas
{
    public class Provincia
    {
        public string Nombre { get; set; }
        public List<Distrito> Distritos { get; set; } = new List<Distrito>();
    }
}
