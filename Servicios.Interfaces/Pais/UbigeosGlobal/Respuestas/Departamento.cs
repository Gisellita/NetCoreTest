﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces.Pais.UbigeosGlobal.Respuestas
{
    public class Departamento
    {
        public string Nombre { get; set; }
        public List<Provincia> Provincias { get; set; } = new List<Provincia>();
    }
}
