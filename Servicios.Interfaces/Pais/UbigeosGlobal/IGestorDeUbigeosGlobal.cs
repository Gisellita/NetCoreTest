﻿using Servicios.Interfaces.Pais.UbigeosGlobal.Respuestas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces.Pais.UbigeosGlobal
{
    public interface IGestorDeUbigeosGlobal
    {
        List<Departamento> GenerarPais();
    }
}
