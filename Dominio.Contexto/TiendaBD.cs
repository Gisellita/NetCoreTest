﻿using Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Contexto
{
    public class TiendaBD : DbContext
    {
        public DbSet<Producto> Productos { set; get; }
        public DbSet<Categoria> Categorias { set; get; }
        public DbSet<Ubigeo> Ubigeos { set; get; }

        public TiendaBD() : base("TiendaDB"){}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
