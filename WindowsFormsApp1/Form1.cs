﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        private HubConnection connection = new HubConnection("http://localhost:52857");
        private IHubProxy hub;

        public Form1()
        {
            this.hub = connection.CreateHubProxy("ChatHub");
            connection.Start();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hub.Invoke("EnviarMensajeGlobal", textBox1.Text);
        }
    }
}
