﻿using Dominio.IntegracionConOtrosServicios.INEI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.ConsolaDePruebas
{
    class Program
    {
        static void Main(string[] args)
        {
            DistribucionPoliticaDePeru.SacarUbigeosDeINEI().ForEach(x => Console.WriteLine(x.NombreDeDistrito));
            Console.ReadKey();
        }
    }
}
